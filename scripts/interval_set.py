import itertools
from collections import namedtuple
import operator
import bisect
import sys
import re


sys.setrecursionlimit(100000)

class Interval(namedtuple('Interval', ['chrom_id', 'start', 'end'])):
    _interval_pattern = re.compile(r'^([^:]+):([0-9]+)-([0-9]+)$')

    @staticmethod
    def parse(string, genome):
        m = re.match(Interval._interval_pattern, string)
        assert m is not None
        chrom_name = m.group(1)
        start = int(m.group(2)) - 1
        end = int(m.group(3))
        return Interval(genome.chrom_id(chrom_name), start, end)

    def to_str(self, genome):
        return '%s:%d-%d' % (genome.chrom_name(self.chrom_id), self.start + 1, self.end)

    def intersects(self, interval):
        return self.chrom_id == interval.chrom_id and self.start < interval.end and self.end > interval.start


Element = namedtuple('Element', ['start', 'end', 'value'])


def _partition(arr, low, high, center):
    new_low = low
    for i in range(low, high):
        if arr[i].end <= center:
            arr[i], arr[new_low] = arr[new_low], arr[i]
            new_low += 1

    new_high = high
    for j in range(high - 1, new_low - 1, -1):
        if arr[j].start > center:
            new_high -= 1
            arr[j], arr[new_high] = arr[new_high], arr[j]

    return new_low, new_high


def _bisect_left_by_key(arr, target, key):
    low = 0
    high = len(arr)

    while low < high:
        mid = (low + high) // 2
        if target > key(arr[mid]):
            low = mid + 1
        else:
            high = mid
    return low


def _bisect_right_by_key(arr, target, key):
    low = 0
    high = len(arr)

    while low < high:
        mid = (low + high) // 2
        if target < key(arr[mid]):
            high = mid
        else:
            low = mid + 1
    return low


class _Node:
    def __init__(self, arr, low, high):
        l = high - low
        if l == 0:
            self._center = 0
            self._by_start = []
            self._by_end = []
            self._left = self._right = None
            return
        self._center = arr[(low + high) // 2].start
        if l == 1:
            self._by_start = [low]
            self._by_end = [low]
            self._left = self._right = None

        new_low, new_high = _partition(arr, low, high, self._center)
        self._by_start = sorted(range(new_low, new_high),
                key=lambda i: arr[i].start)
        self._by_end = sorted(range(new_low, new_high),
                key=lambda i: arr[i].end)
        self._left = None if new_low == low else _Node(arr, low, new_low)
        self._right = None if new_high == high else _Node(arr, new_high, high)

    def after_last_start(self, arr, target):
        return _bisect_right_by_key(self._by_start, target, lambda i: arr[i].start)

    def after_last_end(self, arr, target):
        return _bisect_right_by_key(self._by_end, target, lambda i: arr[i].end)

    def find_by_point(self, arr, target, res_indices):
        if target < self._center:
            i = self.after_last_start(arr, target)
            res_indices.update(self._by_start[:i])
            if self._left is not None:
                self._left.find_by_point(arr, target, res_indices)
        elif target == self._center:
            res_indices.update(self._by_start)
        else:
            i = self.after_last_end(arr, target)
            res_indices.update(self._by_end[i:])
            if self._right is not None:
                self._right.find_by_point(arr, target, res_indices)


class IntervalTree:
    def __init__(self, values):
        self._root = _Node(values, 0, len(values))
        self._starts = sorted((v.start, i) for i, v in enumerate(values))
        self._ends = sorted((v.end, i) for i, v in enumerate(values))
        self._values = values

    def find_by_point(self, target):
        indices = set()
        self._root.find_by_point(self._values, target, indices)
        return [self._values[i] for i in indices]

    def find_by_interval(self, start, end):
        starts_i = _bisect_left_by_key(self._starts, start, operator.itemgetter(0))
        starts_j = _bisect_left_by_key(self._starts, end, operator.itemgetter(0))
        indices = set(map(operator.itemgetter(1),
                          itertools.islice(self._starts, starts_i, starts_j)))

        ends_i = _bisect_right_by_key(self._ends, start, operator.itemgetter(0))
        ends_j = _bisect_right_by_key(self._ends, end, operator.itemgetter(0))
        indices.update(map(operator.itemgetter(1),
                           itertools.islice(self._ends, ends_i, ends_j)))

        self._root.find_by_point(self._values, start, indices)
        return [self._values[i] for i in indices]

    def find_left_bound(self, target):
        i = _bisect_right_by_key(self._ends, target, operator.itemgetter(0))
        if i:
            return self._values[self._ends[i - 1][1]]
        i = _bisect_left_by_key(self._starts, target, operator.itemgetter(0))
        return self._values[self._starts[i - 1][1]] if i else None

    def find_right_bound(self, target):
        i = _bisect_left_by_key(self._starts, target, operator.itemgetter(0))
        l = len(self._values)
        if i < l:
            return self._values[self._starts[i][1]]
        i = _bisect_right_by_key(self._ends, target, operator.itemgetter(0))
        return self._values[self._ends[i][1]] if i < l else None

    def __iter__(self):
        return iter(self._values)


class IntervalSet:
    def __init__(self, pairs):
        """
        arguments:
            pairs: iterator (interval, obj)
        """
        chromosomes = []
        for interval, obj in pairs:
            chrom_id = interval.chrom_id
            if chrom_id >= len(chromosomes):
                chromosomes.extend(
                        [] for _ in range(chrom_id - len(chromosomes) + 1))
            chromosomes[chrom_id].append(Element(interval.start, interval.end, obj))

        self._trees = [IntervalTree(chrom) for chrom in chromosomes]

    def find_all(self, interval):
        chrom_id = interval.chrom_id
        if chrom_id >= len(self._trees):
            return
        res = self._trees[chrom_id].find_by_interval(interval.start, interval.end)
        return map(operator.itemgetter(2), sorted(res, key=operator.itemgetter(0)))

    def find_left_bound(self, interval):
        chrom_id = interval.chrom_id
        if chrom_id >= len(self._trees):
            return None
        res = self._trees[chrom_id].find_left_bound(interval.start)
        if res is None:
            return None
        return res.value if res is not None else None

    def find_right_bound(self, interval):
        chrom_id = interval.chrom_id
        if chrom_id >= len(self._trees):
            return None
        res = self._trees[chrom_id].find_right_bound(interval.end)
        return res.value if res is not None else None

    def __iter__(self):
        return map(operator.itemgetter(2), itertools.chain(*self._trees))
