import pysam

class Names:
    def __init__(self, names, lengths):
        self._names = names
        self._ids = { name: i for i, name in enumerate(names) }
        self._lengths = lengths

    def chrom_id(self, chrom_name):
        return self._ids[chrom_name]

    def chrom_name(self, chrom_id):
        return self._names[chrom_id]

    def chrom_len(self, chrom_id):
        return self._lengths[chrom_id]

    @staticmethod
    def from_pysam(obj):
        """
        From pysam object with fields `references` and `lengths` (for example `FastaFile` or `AlignmentFile`).
        """
        return Names(obj.references, obj.lengths)


class Genome(Names):
    def __init__(self, filename):
        self._fasta_file = pysam.FastaFile(filename)
        super().__init__(self._fasta_file.references, self._fasta_file.lengths)

    def fetch(self, chrom_id, start, end):
        return self._fasta_file.fetch(self.chrom_name(chrom_id), start, end)
