import pysam
import itertools

from scripts.interval_set import Interval, IntervalSet

SAM_REVERSE = 0x10

class Duplication:
    def __init__(self, record, genome):
        region1 = Interval(genome.chrom_id(record.reference_name), record.reference_start, record.reference_end)
        region2 = Interval.parse(record.query_name, genome)
        self._regions = (region1, region2)
        self._strand = not record.is_reverse
        self._record = record
        seq2 = self._record.query_sequence
        self._aligned_pairs = []
        for pos2, pos1, nt1 in self._record.get_aligned_pairs(with_seq=True):
            if pos2 is None:
                self._aligned_pairs.append((pos1, None, nt1, None))
            else:
                true_pos2 = pos2 + self._regions[1].start if self._strand else -(self._regions[1].end - pos2 - 1)
                self._aligned_pairs.append((pos1, true_pos2, nt1, seq2[pos2]))

    def _find_start(self, seq_ind, interval):
        lo = 0
        hi = len(self._aligned_pairs)
        pos = interval.start if seq_ind == 0 or self._strand else -interval.end
        if interval.end < 0:
            print('NEGATIVE STRAND')
        while lo < hi:
            mid = (lo + hi) // 2
            if self._aligned_pairs[mid][seq_ind] < pos:
                lo = mid + 1
            else:
                hi = mid
        return lo

    def region(self, i):
        return self._regions[i]

    def segm_dup_region1(self, genome):
        return Interval.parse(self._record.get_tag('d1'), genome)

    def segm_dup_region2(self, genome):
        return Interval.parse(self._record.get_tag('d2'), genome)

    @property
    def alignment_number(self):
        return int(self._record.get_tag('an'))

    def to_str(self, genome):
        return '{region1}\t{region2}\t{strand}\t{cigar}\t{sd_region1}\t{sd_region2}\t{aln_num}'.format(
            region1=self._regions[0].to_str(genome), region2=self._regions[1].to_str(genome),
            strand='+' if self._strand else '-', aln_num=self.alignment_number, cigar=self._record.cigarstring,
            sd_region1=self._record.get_tag('d1'), sd_region2=self._record.get_tag('d2'))

    def align_variant(self, var_interval, record, genome):
        for seq_ind in range(2):
            if not self._regions[seq_ind].intersects(var_interval):
                continue
            i = self._find_start(seq_ind, var_interval)
            print(record, end='')
            print('    %s  %s' % (self._regions[0].to_str(genome), self._regions[1].to_str(genome)))
            for j in range(max(0, i - 2), min(i + 3, len(self._aligned_pairs))):
                print('    %3d  %d %d %s %s' % ((j,) + self._aligned_pairs[j]))


class DuplicationsSet:
    def __init__(self, reader, genome):
        self._duplications = []
        import tqdm
        for record in tqdm.tqdm(reader):
            self._duplications.append(Duplication(record, genome))
        self._set = IntervalSet(itertools.chain(
            *(((dupl.region(0), i), (dupl.region(1), i)) for i, dupl in enumerate(self._duplications))))
        self._genome = genome

    def analyze_variant(self, record):
        var_interval = Interval(self._genome.chrom_id(record.chrom),
            record.start, record.start + len(record.ref))
        indices = set(self._set.find_all(var_interval))
        if not indices:
            return

        for i in indices:
            self._duplications.align_variant(var_interval, record, self._genome)

