#!/usr/bin/env python3

import argparse
import sys
import csv
import pysam
import gzip
import mappy
import tqdm
import operator
from collections import namedtuple

from _version import __version__


def open_wrap(filename, mode='r'):
    if filename == '-':
        return sys.stdout if mode == 'w' else sys.stdin
    elif filename.endswith('gz'):
        return gzip.open(filename, mode + 't')
    else:
        return open(filename, mode)


def generate_header(genome, chromosomes):
    header = ''
    for name, length in zip(genome.references, genome.lengths):
        if name in chromosomes:
            header += '@SQ\tSN:%s\tLN:%d\n' % (name, length)
    header += '@PG\tID:unreliable\tPN:unreliable\tVN:%s\tCL:%s\n' % (__version__, ' '.join(sys.argv))
    return header


def filter_hits(hits):
    # Currently, does nothing
    return hits


USE_CIGAR = 0x004
NO_SECONDARY = 0x4000
FORWARD_ONLY = 0x100000
REVERSE_ONLY = 0x200000
USE_EQX = 0x4000000

SAM_REVERSE = 0x10
SAM_SUPPLEMENTARY = 0x800


DuplInfo = namedtuple('DuplInfo', 'chrom1 start1 end1 chrom2 start2 end2 strand'.split())


def analyse_pair(dupl, genome, preset, outp):
    seq1 = genome.fetch(dupl.chrom1, dupl.start1, dupl.end1)
    seq2 = genome.fetch(dupl.chrom2, dupl.start2, dupl.end2)

    aligner = mappy.Aligner(seq=seq1, preset=preset)
    flags = NO_SECONDARY# | USE_EQX
    flags |= FORWARD_ONLY if dupl.strand else REVERSE_ONLY
    flags = 0

    hits = list(aligner.map(seq2, extra_flags=flags, MD=True))
    hits = filter_hits(hits)
    for i, hit in enumerate(hits):
        record = pysam.AlignedSegment(header=outp.header)
        record.query_name = '%s:%d-%d' % (dupl.chrom2, dupl.start2 + hit.q_st + 1, dupl.start2 + hit.q_en)
        record.reference_name = dupl.chrom1
        record.reference_start = dupl.start1 + hit.r_st
        record.cigartuples = [(x[1], x[0]) for x in hit.cigar]

        if dupl.strand:
            record.query_sequence = seq2[hit.q_st : hit.q_en]
            record.flag = 0
        else:
            record.query_sequence = mappy.revcomp(seq2[hit.q_st : hit.q_en])
            record.flag = SAM_REVERSE

        if i > 0:
            record.flag |= SAM_SUPPLEMENTARY

        record.set_tag('NM', hit.NM, 'i')
        record.set_tag('MD', hit.MD, 'Z')
        record.set_tag('d1', '%s:%d-%d' % (dupl.chrom1, dupl.start1 + 1, dupl.end1), 'Z')
        record.set_tag('d2', '%s:%d-%d' % (dupl.chrom2, dupl.start2 + 1, dupl.end2), 'Z')
        record.set_tag('an', i, 'i')
        outp.write(record)


def convert_rows(rows):
    res = []
    chromosomes = set()
    seen = set()
    for row in rows:
        chrom1 = row['chrom']
        start1 = int(row['chromStart'])
        end1 = int(row['chromEnd'])
        chrom2 = row['otherChrom']
        start2 = int(row['otherStart'])
        end2 = int(row['otherEnd'])
        strand = row['strand'] == '+'

        key1 = (chrom1, start1, end1, chrom2, start2, end2, strand)
        key2 = (chrom2, start2, end2, chrom1, start1, end1, strand)
        if key1 in seen or key2 in seen:
            continue
        seen.add(key1)
        chromosomes.add(chrom1)
        chromosomes.add(chrom2)
        res.append(DuplInfo(*key1))
    return res, chromosomes


def main():
    parser = argparse.ArgumentParser(
        description='Align all pairs of segmental duplications',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i <csv> -r <fasta> -o <csv> [...]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='FILE', required=True,
        help='Input CSV file with segmental duplications')
    io_args.add_argument('-r', '--reference', metavar='FILE', required=True,
        help='Input FASTA file with genome reference (should be indexed)')
    io_args.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output BAM file with aligned duplications')

    al_args = parser.add_argument_group('Alignment arguments')
    al_args.add_argument('--preset', metavar='STR', default='asm20',
        help='Minimap2 preset [%(default)s]')

    oth_args = parser.add_argument_group('Other arguments')
    oth_args.add_argument('-h', '--help', action='help', help='Show this message and exit')
    oth_args.add_argument('-V', '--version', action='version',
        version='%(prog)s {}\nCreated by Timofey Prodanov and Vikas Bansal'.format(__version__),
        help="Show program's version number and exit")
    args = parser.parse_args()

    duplications = csv.DictReader(open_wrap(args.input), dialect='unix', delimiter='\t')
    genome = pysam.FastaFile(args.reference)
    duplications, chromosomes = convert_rows(duplications)
    outp = pysam.AlignmentFile(args.output, 'wb', text=generate_header(genome, chromosomes))

    for dupl in tqdm.tqdm(duplications):
        analyse_pair(dupl, genome, args.preset, outp)


if __name__ == '__main__':
    main()
